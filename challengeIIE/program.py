#!/usr/bin/env python3

import sys


global_paths = set()


class Node:

    def __init__(self, num, left=None, right=None):
        self.num = num
        self.right = right
        self.left = left

    def __str__(self):
        return 'Node({},{},{})'.format(self.num, self.left, self.right)


def display_path(my_sum, root_node):
    my_path = []
    tree_paths = []
    duplicate_paths = set()
    find_path_r(my_sum, root_node, my_path, 0, tree_paths, duplicate_paths)
    # print("here are my tree paths for this tree ")
    # print(tree_paths)


def find_path_r(target_sum, current_node, my_path, sum_now, tree_paths, duplicate_paths):
    if current_node.num is None:
        return

    sum_now += int(current_node.num)
    my_path.append(current_node.num)

    if sum_now == target_sum:

        print_format_path = [num for num in my_path if int(num) != 0]
        print_format_string = ' '.join(print_format_path)

        if print_format_string in duplicate_paths:
            print("print format string in duplicate_paths")
            pass
        else:
            print("{}:".format(target_sum), end=" ")
            print(*print_format_path, sep=", ", end=" ")

            tree_paths.append(print_format_path)
            duplicate_paths.add(print_format_string)

            print()

    if current_node.left is not None:
        find_path_r(target_sum, current_node.left, my_path, sum_now, tree_paths, duplicate_paths)

    if current_node.right is not None:
        find_path_r(target_sum, current_node.right, my_path, sum_now, tree_paths, duplicate_paths)

    my_path.pop(-1)


def insert_nodes(nodes_array, my_root_node, nodes_array_index, nodes_array_length):

    if nodes_array_index < nodes_array_length:

        if nodes_array[nodes_array_index] == '0':
            new_node = Node(None)
        else:
            new_node = Node(nodes_array[nodes_array_index])

        my_root_node = new_node
        my_root_node.left = insert_nodes(nodes_array, my_root_node.left, 2*nodes_array_index + 1, nodes_array_length)
        my_root_node.right = insert_nodes(nodes_array, my_root_node.right, 2*nodes_array_index + 2, nodes_array_length)

    return my_root_node


if __name__ == '__main__':
    for line in sys.stdin:
        line = line.strip().split()
        if len(line) == 1:
            next_line = sys.stdin.readline().split()
            root = insert_nodes(next_line, None, 0, len(next_line))
            display_path(int(line[0]), root)



