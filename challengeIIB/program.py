#!/usr/bin/env python3

import sys 


INF = float('inf')


def minWeightPath(matrix, rs, cs):

    weightedMat = [[0 for _ in range(cs)] for _ in range(rs)]

    for i in range(rs):
        weightedMat[i][0] += matrix[i][0]

    for j in range(1, cs):
        for i in range(rs):
            if i == 0:      # top row
                weightedMat[i][j] = matrix[i][j] + min(weightedMat[i][j-1], weightedMat[rs-1][j-1], weightedMat[i+1][j-1])
            elif i == rs-1: # bottom row  
                weightedMat[i][j] = matrix[i][j] + min(weightedMat[i][j-1], weightedMat[i-1][j-1], weightedMat[0][j-1])
            else:
                weightedMat[i][j] = matrix[i][j] + min(weightedMat[i][j-1], weightedMat[i-1][j-1], weightedMat[i+1][j-1])


    return weightedMat

def reconstructPath(r, c, mat):


    path = [] 

    path.append(r)

    while c is not 0: 
        if r == 0: 
            if mat[r][c-1]            == min(mat[r][c-1], mat[len(mat)-1][c-1], mat[r+1][c-1]):
                r = r
            elif mat[r+1][c-1]        == min(mat[r][c-1], mat[len(mat)-1][c-1], mat[r+1][c-1]):
                r = r+1
            elif mat[len(mat)-1][c-1] == min(mat[r][c-1], mat[len(mat)-1][c-1], mat[r+1][c-1]):
                r = len(mat)-1

        elif r == len(mat) - 1: 
            if mat[r][c-1]     == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = r
            elif mat[r-1][c-1] == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = r-1
            elif mat[0][c-1]   == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = 0

        else: 
            if mat[r][c-1]   == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = r
            elif mat[r-1][c-1] == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = r-1
            elif mat[r+1][c-1]   == min(mat[r][c-1], mat[r-1][c-1], mat[0][c-1]):
                r = r+1 

        path.append(r+1)
        c -= 1

    return path



if __name__ == '__main__': 
    for line in sys.stdin: 
        rowcol = [int(x) for x in line.strip().split()] 
        row, col = rowcol[0], rowcol[1]
       
        mat = [] 
        for _ in range(row):
            contents = [int(x) for x in sys.stdin.readline().strip().split()]
            mat.append(contents)
        
        minCost = INF
        minRow  = -1
        minPath = [] 
        lastColIdx = len(mat[0])
        
        costMat = minWeightPath(mat, len(mat), lastColIdx)

        for idx in range(len(costMat)): 
            row  = costMat[idx]
            lastElem = row[col-1]

            if lastElem < minCost: 
                minCost = lastElem
                minRow  = idx

        path = reconstructPath(minRow, col-1, costMat)
        path.reverse()
        
        print(minCost)
        print(' '.join([str(x) for x in path]))
 
