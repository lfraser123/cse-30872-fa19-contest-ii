#!/usr/bin/env python3

import sys
from collections import defaultdict
import heapq

'''def findGroups(d, vert, edg):
	if vert == 0:
		return 0, [0]

	count = 0
	sepGroups = 0
	groups = []#[-1]
	current = d[1]
	group = []
	group.append(1)
	while count < vert:
		try:
			group.append(current[0])
			current =  d[current[0]]
			count += 1
		except:
			groups.append(group)
			group  = []
			group.append(count)
			current = d[count]
			group.append(current[0])
			sepGroups +=  1
			count += 1
			
	groups.append(group)
	return  sepGroups, groups'''

def findGroups(d, vert, edg):
	total = vert-edg
	groups = []
	check = [{1}]
	#placed = False
	for edges in  d:
		for c in check:
			if edges in c:
				c.add(d[edges])
				#placed = True
				break
			elif d[edges] in c:
				c.add(edges)
				#placed = True
				break
		#if placed == False:
			else:
				check.append({edges, d[edges]})
		
	return total, check

'''def findGroups(d, vert, edg, path):
	print("in FindGroups")
	print("path:" ,path)
	if len(path) == vert:
		return [path]
	if d[path[-1]] not in d and d[path[-1]] in d.values():
		for i in d.keys():
			if d[i] == d[path[-1]]:
				state = i
		return findGroups(d, vert, edg, path + [state])
	if d[path[-1]] not in d:
		print("return 2")
		return [path]
	meh = d[path[-1]]
	#meh2 = d[meh]
	#d.remove(meh)
	#d.remove(meh2)
	groups = [] + findGroups(d, vert, edg, path + [meh])
	return groups'''
		

'''def findGroups(d, vert, edg, path, count):
	if len(path) == vert:
		return [path]
	if count == edg:
		return [path]
	if d[path[-1]] in d:
		count += 1
		findGroups(d, vert, edg, path+[d[path[-1]]], count)
	elif'''

'''def findGroups(d, vert, edg, path, paths, current):
	if len(path) == vert:
		return path
	print("LEN D: ", len(d))
	while len(d) > 0:
		try:
			print(current)
			path.add(d[current])
			print(path)
			temp = current
			current = d[temp]
			del d[temp]#d[path[-1]]
			print(d)
			print(current)
			break
		except:
			already = False
			current = list(d.keys())[0].value 
			for c in paths:
				if current in c:
					c.add(current)
					already = True
					temp = d[current]
					current = d[temp]
					del d[temp]
					print(d)
					#current = d[temp]
					break
			if already == False:
				paths.append(path)
				path = [k]'''

'''def findGroups(d):
	frontier = []
	visited = {}

	start = 1
		
	#add start to frontier
	heapq.heappush(frontier,  (start, start))
    
    	#process frontier
	while frontier:
		source, target = heapq.heappop(frontier)
		if target in visited:
			continue
		visited[target] = source     # , cost
		for neighbor  in  d[target].items():  #get neighbor and costs
			heapq.heappush(frontier, (target, neighbor))

	return 1, visited'''

'''def findGroups(start, d, visited, path):
	path.append(start)
	if start not in visited:
		visited.append(start)

	for neighbor in d[start]:
		if neighbor not in visited:
			visited.append(neighbor)
			findGroups(neighbor, d, visited)'''
	
			

if __name__ == '__main__':
	count = 0
	for line in sys.stdin:
		d = defaultdict(int)
		vertices = int(line.strip())
		nedges = int(sys.stdin.readline().strip())
		for n in range(nedges):
			x,y = sys.stdin.readline().strip().split(' ')
			x = int(x)
			#y = int(y)
			d[x] = y
			d[y] = x #new to go both ways 
		n, groups =findGroups(d, vertices, nedges)#, {1})#, [[-1]], 1)
		#n = len(groups)
		count += 1
		gr = set()
		for l in groups:
			letter = ''
			for let in l:
				letter = letter + str(let)
			#letter = ''.join(l)
		#	if l in  gr:
		#		continue
		#	else:
			letter = letter.strip()
			gr.add(letter)
		n =  len(gr)
		if n == 1:
			print('Graph {} has 1 group: '.format(count))
		#elif n == 0:
		#	print('Graph {} has 0 groups: '.format(count))
		#	continue
		else:
			print('Graph {} has {} groups:'.format(count, n))
		for l in sorted(gr):
			
			output = ''
			for x in l:
				output = output + str(x)  + ' '
			output = output.strip()
			print(output)
