#!/usr/bin/env python3
# Jack Morgan, Samantha Manfreda, Luke Fraser
# CSE 30872 Programming Challenges
# 11 December 2019
# challengeIIF

import sys


# Function to find gift recipients for a given person
def find_my_recipients(tree, giver, my_spouses):

    my_recipients = []
    visited = set()

    for k, v in tree.items():

        if giver in k:
            continue

        elif my_spouses[giver] in v or giver in v:
            for parent in v:

                if parent in tree:

                    if my_spouses[parent] == giver or parent == giver:
                        continue

                    elif parent in tree and parent not in visited and my_spouses[parent] not in visited:
                        my_recipients += tree[parent]

                    visited.add(parent)

    return my_recipients


# Main Driver
if __name__ == '__main__':

    my_tree = {}
    my_spouses = {}

    for line in sys.stdin:

        line = line.strip().split(':')

        if len(line[0]) < 3:
            continue

        elif len(line) == 2:
            line = [word.split() for word in line]

            my_spouses[line[0][1]] = line[0][0]
            my_spouses[line[0][0]] = line[0][1]

            parents = line[0]
            children = line[1]

            for parent in parents:
                my_tree[parent] = []

                for child in children:
                    my_tree[parent].append(child)

            for child in children:

                if child not in my_spouses:
                    my_spouses[child] = ''

        else:
            giver = line[0]
            recipients_list = sorted(find_my_recipients(my_tree, giver, my_spouses))
            if recipients_list:
                recipients_list = ', '.join(recipients_list)
                print('{} needs to buy gifts for: {}'.format(giver, recipients_list))
            else:
                print('{} does not need to buy gifts'.format(giver))


